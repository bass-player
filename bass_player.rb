require 'qml'
require 'audioinfo'
require './models'

module BassPlayer
  VERSION = '0.1'
  class SongController
    include QML::Access
    register_to_qml

    property(:title) { '' }
    property(:duration) { '' }
    property(:current_song) { '' }
    property(:current_segment) { '' }
    property(:model) { QML::ArrayModel.new(:title, :duration) }
    property(:segment_model) {
      QML::ArrayModel.new(:name, :from, :to)
    }

    def initialize
      super()
      Song.all.each do |song|
        model << {
          title: song.title,
          duration: ms_to_time(song.duration)
        }
      end
    end

    def add(file_url, duration, metadata)
      path = file_url.split('file://').last
      AudioInfo.open(path) do |info|
        title = if info.title.empty?
                  File.basename(path)
                else
                  info.title
                end
        song = Song.create(
          title: title,
          duration: info.length * 1000,
          path: file_url
        )

        item = {
          title: song.title,
          duration: ms_to_time(song.duration)
        }
        model << item
      end
    end

    def song_selected(row)
      unless row.nil?
        self.current_song = model[row][:title]
      end

      segment_model.clear
      self.current_segment = nil

      song = Song.first(title: self.current_song)

      segments = Song.first(title: self.current_song).segments.map do |segment|
        {
          name: segment.name,
          from: ms_to_time(segment.from),
          to: ms_to_time(segment.to)
        }
      end


      for segment in segments
        segment_model << segment
      end
      return {
        path: song.path,
        duration: song.duration
      }
    end

    def segment_selected(row)
      song = Song.first(title: self.current_song)
      self.current_segment = song.segments[row.to_i].to_hash
    end

    def add_segment(name, from, to)
      song = Song.first(title: current_song)
      Segment.create(
        name: name, from: parse_time(from), to: parse_time(to),
        song_id: song.id
      )
      song_selected(nil)
    end

    def ms_to_time(ms)
      retval = Time.at(ms/1000).utc.strftime("%M:%S")
      return retval
    end

    def parse_time(string)
      # Parses a string of the form '3:12' to milliseconds
      min, sec = string.split(':').map(&:to_i)
      return ((min * 60) + sec) * 1000
    end
  end
end

QML.run do |app|
  app.load_path Pathname(__FILE__) + '../main.qml'
end
