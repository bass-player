// main.qml
import QtQuick 2.15
import QtQuick.Controls 1.4
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.3
import QtMultimedia 5.15
import BassPlayer 0.1

ApplicationWindow {
    visible: true
    height: 400
    width: 600

    title: "Bass Player!"

    ColumnLayout {
	anchors.fill: parent
	anchors.margins: 10
	width: 700
	RowLayout {
	    ColumnLayout {
		Label {
		    text: "Songs"
		}
		TableView {
		    id: songTable
		    model: song.model
		    Layout.fillHeight: true
		    Layout.fillWidth: true
		    Layout.alignment: Qt.AlignTop
		    //Layout.minimumWidth: 300
		    alternatingRowColors: true

		    TableViewColumn {
			role: "title"
			title: "Title"
		    }
		    TableViewColumn {
			role: "duration"
			title: "Duration"
		    }
		    onClicked: {
			var tmp = song.song_selected(row)
			audioInterface.source = tmp.path
			durationText.text = song.ms_to_time(tmp.duration)
		    }
		}
		RowLayout {
		    Button {
			icon.name: "cursor-cross"
			onClicked: {
			    fileDialog.open()
			}
		    }
		}
	    }
	    ColumnLayout {
		Label {
		    text: "Segments"
		}
		TableView {
		    id: songPartTable
		    model: song.segment_model
		    Layout.fillHeight: true
		    Layout.fillWidth: true
		    Layout.alignment: Qt.AlignTop
		    //Layout.minimumWidth: 300
		    alternatingRowColors: true

		    TableViewColumn {
			role: "name"
			title: "Name"
		    }
		    TableViewColumn {
			role: "from"
			title: "From"
		    }
		    TableViewColumn {
			role: "to"
			title: "To"
		    }
                    onClicked: {
			song.segment_selected(row)
		    }
		}

		RowLayout {
		    Button {
			icon.name: "cursor-cross"
			onClicked: {
			    addSegmentDialog.open()
			}
		    }
		}
	    }
	}
	FileDialog {
	    id: fileDialog
	    title: "Select a song"
	    folder: shortcuts.music
	    onAccepted: {
		audioInterface.source = fileDialog.fileUrl.toString()
		durationText.text = song.ms_to_time(audioInterface.duration)
		song.add(fileDialog.fileUrl.toString(), audioInterface.duration, audioInterface.metaData)

	    }
	}
	RowLayout {
	    Slider {
		id: slider
		from: 0
		to: 1
		value: 0
		Layout.fillWidth: true
		onMoved: {
		    var position = audioInterface.duration*slider.value
		    audioInterface.seek(position)
		}
	    }
	    Text {
		id: currentPositionText
		text: { '--:--' }
	    }
	    Text { text: '/' }
	    Text {
		id: durationText
		text: { '--:--' }
	    }
	}

	RowLayout {
	    Layout.alignment: Qt.AlignHCenter
	    Button {
		text: "Play"
		icon.name: "media-playback-start"
		onClicked: {
		    audioInterface.play()
		    timer.start()
		}
	    }
	    Button {
		text: "Stop"
		icon.name: "media-playback-stop"
		onClicked: {
		    audioInterface.stop()
		    timer.restart()
		}
	    }
	    Button {
		text: "Pause"
		icon.name: "media-playback-pause"
		onClicked: {
		    audioInterface.pause()
		    timer.stop()
		}
	    }
	}

    }

    SongController {
	id: song
    }

    Audio {
	id: audioInterface
	//audioRole: MusicRole
    }

    Timer {
	id: timer
	running: false
	repeat: true
	onTriggered: {
	    currentPositionText.text = song.ms_to_time(audioInterface.position)
	    slider.value = audioInterface.position / audioInterface.duration

            // Cool segment
            if (song.current_segment) {
                if(audioInterface.position < song.current_segment.from ||
                   audioInterface.position > song.current_segment.to) {
                    audioInterface.seek(song.current_segment.from)
                }
            }
	}
    }

    Dialog {
	id: addSegmentDialog
	title: "Add a segment"
	standardButtons: StandardButton.Save | StandardButton.Cancel

	onAccepted: {
	    song.add_segment(
		textviewName.text,
		textviewFrom.text,
		textviewTo.text
	    )
	}

	ColumnLayout {
	    TextField {
		id: textviewName
		placeholderText: "Name"
	    }
	    TextField {
		id: textviewFrom
		placeholderText: "00:00"
	    }
	    TextField {
		id: textviewTo
		placeholderText: "00:00"
	    }
	}
    }
}
