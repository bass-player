require 'sequel'
DB = Sequel.connect('sqlite:///tmp/bass-player.db')

class Song < Sequel::Model
  one_to_many :segments
end

class Segment < Sequel::Model
  many_to_one :song
end
