require "sequel"

DB = Sequel.connect('sqlite:///tmp/bass-player.db')

DB.create_table(:songs) do
  primary_key :id
  String :title
  Integer :duration
  String :path
end

DB.create_table(:segments) do
  primary_key :id
  String :name
  Integer :from
  Integer :to
  foreign_key :song_id, :songs
end
